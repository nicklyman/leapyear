// business logic
var leapYear = function(year) {
  if ((year % 4 === 0) && (year % 100 !== 0) || (year % 400 === 0)) {
    return true;
  } else {
    return false;
  }
};

// user interface input
$(document).ready(function() {
  $("form#leap-year").submit(function(event) {
    event.preventDefault();
    var year = parseInt($("input#year").val());
    var result = leapYear(year);

    $(".year").text(year);
    if (isNaN(year)) {
      alert("Please enter a valid year.")
      return;
    }
    else if (year < 0) {
      alert("Please enter a number greater than 0.")
      return;
    }
    else if (!result) {
      $(".not").text("not");
    } else {
        $(".not").text("");
    }

    $("#result").show();
  });
});
